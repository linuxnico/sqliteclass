#!/usr/bin/python3
# -*- coding: utf-8 -*- 
#auteur: Nicolas Martin nico.martin@sncf.fr
__version__ = '1'

import os
import sqlite3
debug=False



class sqfile():
    '''
    classe de gestion simplifie d'une base sqlite
    '''
    
    def __init__(self,fichier, table, entete, typechamps):
        '''
        init de la classe
        :param fichier: fichier de la base de donnees
        :param table: tabel de la bdd
        :param entete: entete de colonne
        :param typechamps: type des colonne
        '''
        self.file=fichier
        self.table=table
        self.champs=entete
        self.champsSql="'"+"','".join(self.champs)+"'"
        self.type=typechamps
        if not os.path.isfile(fichier):
            self.creerDB(self.file, self.table, self.champs, self.type)
            

    def creerDB(self, fichier, table, nom, typeChamps):
        '''
        creer un fichier sqlite s'il n'y en pas        
        :param nom: nom de colonnes
        :param typeChamps: type de donnees dans chaque colonne (text, int, double etc)
        :param table: nom de la table
        :param fichier: fichier de la base de donnees
        '''
        if debug: print('on creer la base de donnees')
        conn = sqlite3.connect(fichier)
        cursor = conn.cursor()
        c = ""
        for ind, i in enumerate(nom):
            c += "'{}' {},".format(i,typeChamps[ind])
        c=c[:-1]
        req = """CREATE TABLE IF NOT EXISTS {}(
                id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,{} )""".format(table, c)
        if debug: print(req)
        try:
            cursor.execute(req)
            ret=True
        except:
            ret=False
        conn.commit()
        conn.close()
        return ret
    
    def tabVirgule(self,donnees):
        '''
        ;ets en forme avec des virgule et de guillemets si c'est du texte
        :param donnees: list ou list de list
        :param typ: type de colonnes
        '''
        r=''
        for ind,i in enumerate(donnees):
            if self.champs[ind].upper()=='TEXT':
                i='\''+i+'\''
            r+=str(i)+','
        return r[:-1]
    
    def add(self, donnees):
        '''
        ajouter un enregistrement a la base
        :param donnees: donnees a inserer
        '''
        if debug: print('on ajoute un enregistrement')        
        conn = sqlite3.connect(self.file)
        cursor = conn.cursor()
        if isinstance(donnees[0],list):
            if debug: print("les donnees sont un tableau de list")
            for d in donnees:
                if len(d)==len(self.champs):
                    requete = """INSERT INTO {}({}) VALUES({})""".format(self.table, self.champsSql,self.tabVirgule(d))
                    if debug: print(requete)
                    cursor.execute(requete)
                else:
                    return False
        else:
            if debug: print("les donnees sont juste une liste")
            if len(donnees)==len(self.champs):
                requete = """INSERT INTO {}({}) VALUES({})""".format(table, self.champsSql,self.tabVirgule(donnees))
                if debug: print(requete)
                cursor.execute(requete)
            else:
                return False
        conn.commit()
        conn.close()
        return True
    
    
    def delete(self, indiceUnique):
        '''
        supprime un enregistrement
        :param indiceUnique: indice unique de la ligne
        '''
        if debug: print('on supprime un enregistrement') 
        conn = sqlite3.connect(self.file)
        cursor = conn.cursor()
        requete = """DELETE FROM {} WHERE \"id\"=\"{}\"""".format(self.table, indiceUnique)
        if debug: print(requete)
        try:
            cursor.execute(requete)
            ret=True
        except:
            ret=False
        conn.commit()
        conn.close()
        return ret
    
    
    def search(self, colonne="*", donnee="*"):
        '''
        recherche un enregistrement 
        si colonne et donnees sont omis, recupere toutes les donnees
        :param colonne: colonne de recherche
        :param donnee: donnee a chercher
        '''        
        if debug: print('on recherche un enregistrement') 
        conn = sqlite3.connect(self.file)
        cursor = conn.cursor()
        requete = """SELECT * FROM {} WHERE \"{}\"=\"{}\"""".format(self.table, colonne, donnee)
        if debug: print(requete)
        cursor.execute(requete)
        rows = cursor.fetchall()
        conn.commit()
        conn.close()
        return rows
    
    def update(self, colonne,donnee, indiceUnique):
        '''
        modifie une colonne d'un enregistrement
        :param colonne: colonne a modifier (peut etre une list)
        :param donnee: donnees (peut etre une list)
        :param indiceUnique: indice unique de la ligne
        '''
        conn = sqlite3.connect(self.file)
        cursor = conn.cursor()
        valeurs = ""
        if isinstance(colonne, list) and len(colonne)==len(donnee):
            for i in range(0, len(donnee)):
                valeurs += '"{}"="{}",'.format(colonne[i], donnee[i])
            valeurs = valeurs[:-1]
        else:
            valeurs='"{}"="{}"'.format(colonne,donnee)
        requete = """UPDATE {} SET {} WHERE "id"={} """.format(self.table, valeurs, indiceUnique)
        if debug: print(requete)
        try:
            cursor.execute(requete)
            ret=True
        except:
            ret=False   
        conn.commit()
        conn.close()
        return ret     
        
        
    def requete(self, requete):
        '''
        execute une requete brute
        :param requete: requete brute
        '''
        if debug: print('on fait une requete brute') 
        try:
            conn = sqlite3.connect(self.file)
            cursor = conn.cursor()
            if debug: print(requete)
            cursor.execute(requete)
            rows = cursor.fetchall()
            conn.commit()
            conn.close()       
            return rows or True
        except:
            return None
        

if __name__ == '__main__':    
    #===============================================================================
    # pour test
    #===============================================================================
    debug=True
    entete=['1','2','3','4']
    typeChamps=['TEXT','INT','TEXT','TEXT']
    donneessimple=['10','20','30','40']
    donneescomplexes=[['1',2,'3','4'],['5',6,'7','8'],['9','10','11','12']]
    table='donnees'
    fichier='sqbase.db'
    test=sqfile('sqbase.db', 'donnees', entete, typeChamps)
    print(test.add(donneessimple))
    print(test.add(donneescomplexes))
    print(test.delete(5))
    print(test.search("*", "*"))
    print(test.update('2','200',4))
    print(test.requete("""SELECT * FROM 'donnees' WHERE "2">100"""))
    