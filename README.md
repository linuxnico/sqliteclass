# sqliteClass

Une classe plus sympa d'utilisation de sqlite en python

###Necessite:

* Python 3
* Sqlite3

###Usage:


**on importe le module**    
from sqliteClass import sqfile

**on creer ou ouvre la base de donnee sqlite**    
test=sqfile('sqbase.db', 'donnees', ['a','b','c','d'], ['TEXT','INT','TEXT','TEXT'])

**on ajoute plusieurs enregistrements**    
print(test.add([['1',2,'3','4'],['5',6,'7','8'],['9','10','11','12']])    
**retour:** True	

**on ajoute un seul  enregistrement**	
print(test.add(['9','10','11','12'])    
**retour:** True

**on supprime un enregistrement**	
print(test.delete(5))    
**retour:** True

**on recherche un enregistrement ou tous les enrgeistrements**	
print(test.search("id", "1"))    
**retour:**[(1, u'9', 10, u'11', u'12')]

**on mets a jour un enregistrement (juste la colonne 2 de l'id 1)**	    
print(test.update('b',200,1))    
**retour:** True

**on cree une requete a la main**	
print(test.requete("""SELECT * FROM 'donnees' WHERE "b">100"""))    
**retour:**[(1, u'9', 200, u'11', u'12')]
