from distutils.core import setup

setup(
    name='sqliteClass',
    version='0.1',
    packages=['sqliteClass',],
    license='GPL v3 license',
    long_description=open('README.md').read(),
)